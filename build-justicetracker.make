api = 2
core = 7.x

; Include the definition for how to build Drupal core directly, including patches:
; This is the 'should' way, but our jenkins server does drush make http:// and that
; does not work with includes. So we patch from within this file
;includes[] = drupal-org-core.make

projects[drupal][type] = core
projects[drupal][version] = 7.35

; Patches for Core
;

; Remove xmlrpc.php for security reasons since we are not using rpc on justicetracker
projects[drupal][patch][] = https://www.drupal.org/files/issues/2430081-remove-xmlrpc.php-7.34.patch

; Pull the devtrac project from git
projects[devtrac][type] = "profile"
projects[devtrac][download][type] = "git"
projects[devtrac][download][branch] = "7.x-1.x"
projects[devtrac][download][url]  = "http://git.drupal.org/sandbox/batje/2456887.git"
