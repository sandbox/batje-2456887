<?php
/**
 * @file
 * justicetracker_feature.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function justicetracker_feature_taxonomy_default_vocabularies() {
  return array(
    'city' => array(
      'name' => 'City',
      'machine_name' => 'city',
      'description' => 'Tags sourced from OpenCalais about City.',
      'hierarchy' => 0,
      'module' => 'opencalais',
      'weight' => 0,
      'rdf_mapping' => array(
        'rdftype' => array(
          0 => 'skos:ConceptScheme',
        ),
        'name' => array(
          'predicates' => array(
            0 => 'dc:title',
          ),
        ),
        'description' => array(
          'predicates' => array(
            0 => 'rdfs:comment',
          ),
        ),
      ),
    ),
    'company' => array(
      'name' => 'Company',
      'machine_name' => 'company',
      'description' => 'Tags sourced from OpenCalais about Company.',
      'hierarchy' => 0,
      'module' => 'opencalais',
      'weight' => 0,
      'rdf_mapping' => array(
        'rdftype' => array(
          0 => 'skos:ConceptScheme',
        ),
        'name' => array(
          'predicates' => array(
            0 => 'dc:title',
          ),
        ),
        'description' => array(
          'predicates' => array(
            0 => 'rdfs:comment',
          ),
        ),
      ),
    ),
    'events_facts' => array(
      'name' => 'Events Facts',
      'machine_name' => 'events_facts',
      'description' => 'Tags sourced from OpenCalais about Events Facts.',
      'hierarchy' => 0,
      'module' => 'opencalais',
      'weight' => 0,
      'rdf_mapping' => array(
        'rdftype' => array(
          0 => 'skos:ConceptScheme',
        ),
        'name' => array(
          'predicates' => array(
            0 => 'dc:title',
          ),
        ),
        'description' => array(
          'predicates' => array(
            0 => 'rdfs:comment',
          ),
        ),
      ),
    ),
    'facility' => array(
      'name' => 'Facility',
      'machine_name' => 'facility',
      'description' => 'Tags sourced from OpenCalais about Facility.',
      'hierarchy' => 0,
      'module' => 'opencalais',
      'weight' => 0,
      'rdf_mapping' => array(
        'rdftype' => array(
          0 => 'skos:ConceptScheme',
        ),
        'name' => array(
          'predicates' => array(
            0 => 'dc:title',
          ),
        ),
        'description' => array(
          'predicates' => array(
            0 => 'rdfs:comment',
          ),
        ),
      ),
    ),
    'organization' => array(
      'name' => 'Organization',
      'machine_name' => 'organization',
      'description' => 'Tags sourced from OpenCalais about Organization.',
      'hierarchy' => 0,
      'module' => 'opencalais',
      'weight' => 0,
      'rdf_mapping' => array(
        'rdftype' => array(
          0 => 'skos:ConceptScheme',
        ),
        'name' => array(
          'predicates' => array(
            0 => 'dc:title',
          ),
        ),
        'description' => array(
          'predicates' => array(
            0 => 'rdfs:comment',
          ),
        ),
      ),
    ),
    'person' => array(
      'name' => 'Person',
      'machine_name' => 'person',
      'description' => 'Tags sourced from OpenCalais about Person.',
      'hierarchy' => 0,
      'module' => 'opencalais',
      'weight' => 0,
      'rdf_mapping' => array(
        'rdftype' => array(
          0 => 'skos:ConceptScheme',
        ),
        'name' => array(
          'predicates' => array(
            0 => 'dc:title',
          ),
        ),
        'description' => array(
          'predicates' => array(
            0 => 'rdfs:comment',
          ),
        ),
      ),
    ),
    'phone_number' => array(
      'name' => 'Phone Number',
      'machine_name' => 'phone_number',
      'description' => 'Tags sourced from OpenCalais about Phone Number.',
      'hierarchy' => 0,
      'module' => 'opencalais',
      'weight' => 0,
      'rdf_mapping' => array(
        'rdftype' => array(
          0 => 'skos:ConceptScheme',
        ),
        'name' => array(
          'predicates' => array(
            0 => 'dc:title',
          ),
        ),
        'description' => array(
          'predicates' => array(
            0 => 'rdfs:comment',
          ),
        ),
      ),
    ),
    'position' => array(
      'name' => 'Position',
      'machine_name' => 'position',
      'description' => 'Tags sourced from OpenCalais about Position.',
      'hierarchy' => 0,
      'module' => 'opencalais',
      'weight' => 0,
      'rdf_mapping' => array(
        'rdftype' => array(
          0 => 'skos:ConceptScheme',
        ),
        'name' => array(
          'predicates' => array(
            0 => 'dc:title',
          ),
        ),
        'description' => array(
          'predicates' => array(
            0 => 'rdfs:comment',
          ),
        ),
      ),
    ),
    'province_or_state' => array(
      'name' => 'Province Or State',
      'machine_name' => 'province_or_state',
      'description' => 'Tags sourced from OpenCalais about Province Or State.',
      'hierarchy' => 0,
      'module' => 'opencalais',
      'weight' => 0,
      'rdf_mapping' => array(
        'rdftype' => array(
          0 => 'skos:ConceptScheme',
        ),
        'name' => array(
          'predicates' => array(
            0 => 'dc:title',
          ),
        ),
        'description' => array(
          'predicates' => array(
            0 => 'rdfs:comment',
          ),
        ),
      ),
    ),
    'tags' => array(
      'name' => 'Tags',
      'machine_name' => 'tags',
      'description' => 'Use tags to group articles on similar topics into categories.',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
      'rdf_mapping' => array(
        'rdftype' => array(
          0 => 'skos:ConceptScheme',
        ),
        'name' => array(
          'predicates' => array(
            0 => 'dc:title',
          ),
        ),
        'description' => array(
          'predicates' => array(
            0 => 'rdfs:comment',
          ),
        ),
      ),
    ),
  );
}
