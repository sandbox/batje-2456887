<?php
/**
 * @file
 * justicetracker_feature.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function justicetracker_feature_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'court_cases_search';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'search_api_index_court_cases_index';
  $view->human_name = 'Court Cases Search';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Court Cases Search';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'none';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '10';
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['columns'] = array(
    'nid' => 'nid',
    'title' => 'title',
  );
  $handler->display->display_options['style_options']['default'] = '-1';
  $handler->display->display_options['style_options']['info'] = array(
    'nid' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'title' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
  );
  /* Field: Indexed Node: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'search_api_index_court_cases_index';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['title']['link_to_entity'] = 1;
  /* Filter criterion: Indexed Node: Facility */
  $handler->display->display_options['filters']['opencalais_facility_tags']['id'] = 'opencalais_facility_tags';
  $handler->display->display_options['filters']['opencalais_facility_tags']['table'] = 'search_api_index_court_cases_index';
  $handler->display->display_options['filters']['opencalais_facility_tags']['field'] = 'opencalais_facility_tags';
  $handler->display->display_options['filters']['opencalais_facility_tags']['value'] = array();
  $handler->display->display_options['filters']['opencalais_facility_tags']['exposed'] = TRUE;
  $handler->display->display_options['filters']['opencalais_facility_tags']['expose']['operator_id'] = 'opencalais_facility_tags_op';
  $handler->display->display_options['filters']['opencalais_facility_tags']['expose']['label'] = 'Facility';
  $handler->display->display_options['filters']['opencalais_facility_tags']['expose']['operator'] = 'opencalais_facility_tags_op';
  $handler->display->display_options['filters']['opencalais_facility_tags']['expose']['identifier'] = 'opencalais_facility_tags';
  $handler->display->display_options['filters']['opencalais_facility_tags']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
  );
  $handler->display->display_options['filters']['opencalais_facility_tags']['type'] = 'select';

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['path'] = 'court-cases-search';
  $handler->display->display_options['menu']['type'] = 'normal';
  $handler->display->display_options['menu']['title'] = 'Court Cases';
  $handler->display->display_options['menu']['name'] = 'main-menu';
  $export['court_cases_search'] = $view;

  return $export;
}
