<?php
/**
 * @file
 * justicetracker_feature.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function justicetracker_feature_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance: 'node-court_case-body'
  $field_instances['node-court_case-body'] = array(
    'bundle' => 'court_case',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 0,
      ),
      'teaser' => array(
        'label' => 'hidden',
        'module' => 'text',
        'settings' => array(
          'trim_length' => 600,
        ),
        'type' => 'text_summary_or_trimmed',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'body',
    'label' => 'Body',
    'required' => FALSE,
    'settings' => array(
      'display_summary' => TRUE,
      'text_processing' => 1,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'module' => 'text',
      'settings' => array(
        'rows' => 20,
        'summary_rows' => 5,
      ),
      'type' => 'text_textarea_with_summary',
      'weight' => -4,
    ),
  );

  // Exported field_instance: 'node-court_case-field_tags'
  $field_instances['node-court_case-field_tags'] = array(
    'bundle' => 'court_case',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'taxonomy',
        'settings' => array(),
        'type' => 'taxonomy_term_reference_link',
        'weight' => 1,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_tags',
    'label' => 'Tags',
    'required' => 0,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 0,
      'module' => 'taxonomy',
      'settings' => array(
        'autocomplete_path' => 'taxonomy/autocomplete',
        'size' => 60,
      ),
      'type' => 'taxonomy_autocomplete',
      'weight' => -2,
    ),
  );

  // Exported field_instance: 'node-court_case-opencalais_city_tags'
  $field_instances['node-court_case-opencalais_city_tags'] = array(
    'bundle' => 'court_case',
    'default_formatter' => 'taxonomy_term_reference_link',
    'default_value' => NULL,
    'default_widget' => 'options_select',
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'taxonomy',
        'settings' => array(),
        'type' => 'taxonomy_term_reference_link',
        'weight' => 47,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'opencalais_city_tags',
    'label' => 'City',
    'required' => FALSE,
    'settings' => array(
      'allowed_values' => array(
        0 => array(
          'vocabulary' => '',
          'parent' => 0,
        ),
      ),
      'opencalais' => 'city',
      'threshold' => 0,
      'user_register_form' => FALSE,
    ),
    'title' => 'City',
    'widget' => array(
      'module' => 'taxonomy',
      'settings' => array(
        'autocomplete_path' => 'taxonomy/autocomplete',
        'size' => 60,
      ),
      'type' => 'taxonomy_autocomplete',
      'weight' => 44,
    ),
  );

  // Exported field_instance: 'node-court_case-opencalais_company_tags'
  $field_instances['node-court_case-opencalais_company_tags'] = array(
    'bundle' => 'court_case',
    'default_formatter' => 'taxonomy_term_reference_link',
    'default_value' => NULL,
    'default_widget' => 'options_select',
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'taxonomy',
        'settings' => array(),
        'type' => 'taxonomy_term_reference_link',
        'weight' => 48,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'opencalais_company_tags',
    'label' => 'Company',
    'required' => FALSE,
    'settings' => array(
      'allowed_values' => array(
        0 => array(
          'vocabulary' => '',
          'parent' => 0,
        ),
      ),
      'opencalais' => 'company',
      'threshold' => 0,
      'user_register_form' => FALSE,
    ),
    'title' => 'Company',
    'widget' => array(
      'module' => 'taxonomy',
      'settings' => array(
        'autocomplete_path' => 'taxonomy/autocomplete',
        'size' => 60,
      ),
      'type' => 'taxonomy_autocomplete',
      'weight' => 45,
    ),
  );

  // Exported field_instance: 'node-court_case-opencalais_eventsfacts_tags'
  $field_instances['node-court_case-opencalais_eventsfacts_tags'] = array(
    'bundle' => 'court_case',
    'default_formatter' => 'taxonomy_term_reference_link',
    'default_value' => NULL,
    'default_widget' => 'options_select',
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'taxonomy',
        'settings' => array(),
        'type' => 'taxonomy_term_reference_link',
        'weight' => 49,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'opencalais_eventsfacts_tags',
    'label' => 'EventsFacts',
    'required' => FALSE,
    'settings' => array(
      'allowed_values' => array(
        0 => array(
          'vocabulary' => '',
          'parent' => 0,
        ),
      ),
      'opencalais' => 'events_facts',
      'threshold' => 0,
      'user_register_form' => FALSE,
    ),
    'title' => 'EventsFacts',
    'widget' => array(
      'module' => 'taxonomy',
      'settings' => array(
        'autocomplete_path' => 'taxonomy/autocomplete',
        'size' => 60,
      ),
      'type' => 'taxonomy_autocomplete',
      'weight' => 46,
    ),
  );

  // Exported field_instance: 'node-court_case-opencalais_facility_tags'
  $field_instances['node-court_case-opencalais_facility_tags'] = array(
    'bundle' => 'court_case',
    'default_formatter' => 'taxonomy_term_reference_link',
    'default_value' => NULL,
    'default_widget' => 'options_select',
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'taxonomy',
        'settings' => array(),
        'type' => 'taxonomy_term_reference_link',
        'weight' => 50,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'opencalais_facility_tags',
    'label' => 'Facility',
    'required' => FALSE,
    'settings' => array(
      'allowed_values' => array(
        0 => array(
          'vocabulary' => '',
          'parent' => 0,
        ),
      ),
      'opencalais' => 'facility',
      'threshold' => 0,
      'user_register_form' => FALSE,
    ),
    'title' => 'Facility',
    'widget' => array(
      'module' => 'taxonomy',
      'settings' => array(
        'autocomplete_path' => 'taxonomy/autocomplete',
        'size' => 60,
      ),
      'type' => 'taxonomy_autocomplete',
      'weight' => 47,
    ),
  );

  // Exported field_instance: 'node-court_case-opencalais_organization_tags'
  $field_instances['node-court_case-opencalais_organization_tags'] = array(
    'bundle' => 'court_case',
    'default_formatter' => 'taxonomy_term_reference_link',
    'default_value' => NULL,
    'default_widget' => 'options_select',
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'taxonomy',
        'settings' => array(),
        'type' => 'taxonomy_term_reference_link',
        'weight' => 51,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'opencalais_organization_tags',
    'label' => 'Organization',
    'required' => FALSE,
    'settings' => array(
      'allowed_values' => array(
        0 => array(
          'vocabulary' => '',
          'parent' => 0,
        ),
      ),
      'opencalais' => 'organization',
      'threshold' => 0,
      'user_register_form' => FALSE,
    ),
    'title' => 'Organization',
    'widget' => array(
      'module' => 'taxonomy',
      'settings' => array(
        'autocomplete_path' => 'taxonomy/autocomplete',
        'size' => 60,
      ),
      'type' => 'taxonomy_autocomplete',
      'weight' => 48,
    ),
  );

  // Exported field_instance: 'node-court_case-opencalais_person_tags'
  $field_instances['node-court_case-opencalais_person_tags'] = array(
    'bundle' => 'court_case',
    'default_formatter' => 'taxonomy_term_reference_link',
    'default_value' => NULL,
    'default_widget' => 'options_select',
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'taxonomy',
        'settings' => array(),
        'type' => 'taxonomy_term_reference_link',
        'weight' => 52,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'opencalais_person_tags',
    'label' => 'Person',
    'required' => FALSE,
    'settings' => array(
      'allowed_values' => array(
        0 => array(
          'vocabulary' => '',
          'parent' => 0,
        ),
      ),
      'opencalais' => 'person',
      'threshold' => 0,
      'user_register_form' => FALSE,
    ),
    'title' => 'Person',
    'widget' => array(
      'module' => 'taxonomy',
      'settings' => array(
        'autocomplete_path' => 'taxonomy/autocomplete',
        'size' => 60,
      ),
      'type' => 'taxonomy_autocomplete',
      'weight' => 49,
    ),
  );

  // Exported field_instance: 'node-court_case-opencalais_phonenumber_tags'
  $field_instances['node-court_case-opencalais_phonenumber_tags'] = array(
    'bundle' => 'court_case',
    'default_formatter' => 'taxonomy_term_reference_link',
    'default_value' => NULL,
    'default_widget' => 'options_select',
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'taxonomy',
        'settings' => array(),
        'type' => 'taxonomy_term_reference_link',
        'weight' => 53,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'opencalais_phonenumber_tags',
    'label' => 'PhoneNumber',
    'required' => FALSE,
    'settings' => array(
      'allowed_values' => array(
        0 => array(
          'vocabulary' => '',
          'parent' => 0,
        ),
      ),
      'opencalais' => 'phone_number',
      'threshold' => 0,
      'user_register_form' => FALSE,
    ),
    'title' => 'PhoneNumber',
    'widget' => array(
      'module' => 'taxonomy',
      'settings' => array(
        'autocomplete_path' => 'taxonomy/autocomplete',
        'size' => 60,
      ),
      'type' => 'taxonomy_autocomplete',
      'weight' => 50,
    ),
  );

  // Exported field_instance: 'node-court_case-opencalais_position_tags'
  $field_instances['node-court_case-opencalais_position_tags'] = array(
    'bundle' => 'court_case',
    'default_formatter' => 'taxonomy_term_reference_link',
    'default_value' => NULL,
    'default_widget' => 'options_select',
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'taxonomy',
        'settings' => array(),
        'type' => 'taxonomy_term_reference_link',
        'weight' => 54,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'opencalais_position_tags',
    'label' => 'Position',
    'required' => FALSE,
    'settings' => array(
      'allowed_values' => array(
        0 => array(
          'vocabulary' => '',
          'parent' => 0,
        ),
      ),
      'opencalais' => 'position',
      'threshold' => 0,
      'user_register_form' => FALSE,
    ),
    'title' => 'Position',
    'widget' => array(
      'module' => 'taxonomy',
      'settings' => array(
        'autocomplete_path' => 'taxonomy/autocomplete',
        'size' => 60,
      ),
      'type' => 'taxonomy_autocomplete',
      'weight' => 51,
    ),
  );

  // Exported field_instance: 'node-court_case-opencalais_provinceorstate_tags'
  $field_instances['node-court_case-opencalais_provinceorstate_tags'] = array(
    'bundle' => 'court_case',
    'default_formatter' => 'taxonomy_term_reference_link',
    'default_value' => NULL,
    'default_widget' => 'options_select',
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'taxonomy',
        'settings' => array(),
        'type' => 'taxonomy_term_reference_link',
        'weight' => 55,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'opencalais_provinceorstate_tags',
    'label' => 'ProvinceOrState',
    'required' => FALSE,
    'settings' => array(
      'allowed_values' => array(
        0 => array(
          'vocabulary' => '',
          'parent' => 0,
        ),
      ),
      'opencalais' => 'province_or_state',
      'threshold' => 0,
      'user_register_form' => FALSE,
    ),
    'title' => 'ProvinceOrState',
    'widget' => array(
      'module' => 'taxonomy',
      'settings' => array(
        'autocomplete_path' => 'taxonomy/autocomplete',
        'size' => 60,
      ),
      'type' => 'taxonomy_autocomplete',
      'weight' => 52,
    ),
  );

  // Exported field_instance: 'taxonomy_term-city-containedbycounty'
  $field_instances['taxonomy_term-city-containedbycounty'] = array(
    'bundle' => 'city',
    'default_formatter' => 'text_default',
    'default_value' => NULL,
    'default_widget' => 'text_textfield',
    'deleted' => 0,
    'description' => 'Tags sourced from OpenCalais about City.',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 3,
      ),
    ),
    'entity_type' => 'taxonomy_term',
    'field_name' => 'containedbycounty',
    'instance_settings' => array(
      'text_processing' => 0,
    ),
    'label' => 'OpenCalais Metadata: ContainedByCounty',
    'required' => FALSE,
    'settings' => array(
      'max_length' => 255,
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'title' => 'ContainedByCounty',
    'widget' => array(
      'module' => 'text',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => 4,
    ),
  );

  // Exported field_instance: 'taxonomy_term-city-containedbystate'
  $field_instances['taxonomy_term-city-containedbystate'] = array(
    'bundle' => 'city',
    'default_formatter' => 'text_default',
    'default_value' => NULL,
    'default_widget' => 'text_textfield',
    'deleted' => 0,
    'description' => 'Tags sourced from OpenCalais about City.',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 2,
      ),
    ),
    'entity_type' => 'taxonomy_term',
    'field_name' => 'containedbystate',
    'instance_settings' => array(
      'text_processing' => 0,
    ),
    'label' => 'OpenCalais Metadata: ContainedByState',
    'required' => FALSE,
    'settings' => array(
      'max_length' => 255,
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'title' => 'ContainedByState',
    'widget' => array(
      'module' => 'text',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => 3,
    ),
  );

  // Exported field_instance: 'taxonomy_term-city-latitude'
  $field_instances['taxonomy_term-city-latitude'] = array(
    'bundle' => 'city',
    'default_formatter' => 'text_default',
    'default_value' => NULL,
    'default_widget' => 'text_textfield',
    'deleted' => 0,
    'description' => 'Tags sourced from OpenCalais about City.',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'taxonomy_term',
    'field_name' => 'latitude',
    'instance_settings' => array(
      'text_processing' => 0,
    ),
    'label' => 'OpenCalais Metadata: Latitude',
    'required' => FALSE,
    'settings' => array(
      'max_length' => 255,
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'title' => 'Latitude',
    'widget' => array(
      'module' => 'text',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => 1,
    ),
  );

  // Exported field_instance: 'taxonomy_term-city-longitude'
  $field_instances['taxonomy_term-city-longitude'] = array(
    'bundle' => 'city',
    'default_formatter' => 'text_default',
    'default_value' => NULL,
    'default_widget' => 'text_textfield',
    'deleted' => 0,
    'description' => 'Tags sourced from OpenCalais about City.',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 1,
      ),
    ),
    'entity_type' => 'taxonomy_term',
    'field_name' => 'longitude',
    'instance_settings' => array(
      'text_processing' => 0,
    ),
    'label' => 'OpenCalais Metadata: Longitude',
    'required' => FALSE,
    'settings' => array(
      'max_length' => 255,
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'title' => 'Longitude',
    'widget' => array(
      'module' => 'text',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => 2,
    ),
  );

  // Exported field_instance: 'taxonomy_term-company-legalname'
  $field_instances['taxonomy_term-company-legalname'] = array(
    'bundle' => 'company',
    'default_formatter' => 'text_default',
    'default_value' => NULL,
    'default_widget' => 'text_textfield',
    'deleted' => 0,
    'description' => 'Tags sourced from OpenCalais about Company.',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 1,
      ),
    ),
    'entity_type' => 'taxonomy_term',
    'field_name' => 'legalname',
    'instance_settings' => array(
      'text_processing' => 0,
    ),
    'label' => 'OpenCalais Metadata: LegalName',
    'required' => FALSE,
    'settings' => array(
      'max_length' => 255,
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'title' => 'LegalName',
    'widget' => array(
      'module' => 'text',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => 2,
    ),
  );

  // Exported field_instance: 'taxonomy_term-company-ticker'
  $field_instances['taxonomy_term-company-ticker'] = array(
    'bundle' => 'company',
    'default_formatter' => 'text_default',
    'default_value' => NULL,
    'default_widget' => 'text_textfield',
    'deleted' => 0,
    'description' => 'Tags sourced from OpenCalais about Company.',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'taxonomy_term',
    'field_name' => 'ticker',
    'instance_settings' => array(
      'text_processing' => 0,
    ),
    'label' => 'OpenCalais Metadata: Ticker',
    'required' => FALSE,
    'settings' => array(
      'max_length' => 255,
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'title' => 'Ticker',
    'widget' => array(
      'module' => 'text',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => 1,
    ),
  );

  // Exported field_instance: 'taxonomy_term-province_or_state-containedbycounty'
  $field_instances['taxonomy_term-province_or_state-containedbycounty'] = array(
    'bundle' => 'province_or_state',
    'default_formatter' => 'text_default',
    'default_value' => NULL,
    'default_widget' => 'text_textfield',
    'deleted' => 0,
    'description' => 'Tags sourced from OpenCalais about Province Or State.',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 3,
      ),
    ),
    'entity_type' => 'taxonomy_term',
    'field_name' => 'containedbycounty',
    'instance_settings' => array(
      'text_processing' => 0,
    ),
    'label' => 'OpenCalais Metadata: ContainedByCounty',
    'required' => FALSE,
    'settings' => array(
      'max_length' => 255,
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'title' => 'ContainedByCounty',
    'widget' => array(
      'module' => 'text',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => 4,
    ),
  );

  // Exported field_instance: 'taxonomy_term-province_or_state-containedbystate'
  $field_instances['taxonomy_term-province_or_state-containedbystate'] = array(
    'bundle' => 'province_or_state',
    'default_formatter' => 'text_default',
    'default_value' => NULL,
    'default_widget' => 'text_textfield',
    'deleted' => 0,
    'description' => 'Tags sourced from OpenCalais about Province Or State.',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 2,
      ),
    ),
    'entity_type' => 'taxonomy_term',
    'field_name' => 'containedbystate',
    'instance_settings' => array(
      'text_processing' => 0,
    ),
    'label' => 'OpenCalais Metadata: ContainedByState',
    'required' => FALSE,
    'settings' => array(
      'max_length' => 255,
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'title' => 'ContainedByState',
    'widget' => array(
      'module' => 'text',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => 3,
    ),
  );

  // Exported field_instance: 'taxonomy_term-province_or_state-latitude'
  $field_instances['taxonomy_term-province_or_state-latitude'] = array(
    'bundle' => 'province_or_state',
    'default_formatter' => 'text_default',
    'default_value' => NULL,
    'default_widget' => 'text_textfield',
    'deleted' => 0,
    'description' => 'Tags sourced from OpenCalais about Province Or State.',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'taxonomy_term',
    'field_name' => 'latitude',
    'instance_settings' => array(
      'text_processing' => 0,
    ),
    'label' => 'OpenCalais Metadata: Latitude',
    'required' => FALSE,
    'settings' => array(
      'max_length' => 255,
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'title' => 'Latitude',
    'widget' => array(
      'module' => 'text',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => 1,
    ),
  );

  // Exported field_instance: 'taxonomy_term-province_or_state-longitude'
  $field_instances['taxonomy_term-province_or_state-longitude'] = array(
    'bundle' => 'province_or_state',
    'default_formatter' => 'text_default',
    'default_value' => NULL,
    'default_widget' => 'text_textfield',
    'deleted' => 0,
    'description' => 'Tags sourced from OpenCalais about Province Or State.',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 1,
      ),
    ),
    'entity_type' => 'taxonomy_term',
    'field_name' => 'longitude',
    'instance_settings' => array(
      'text_processing' => 0,
    ),
    'label' => 'OpenCalais Metadata: Longitude',
    'required' => FALSE,
    'settings' => array(
      'max_length' => 255,
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'title' => 'Longitude',
    'widget' => array(
      'module' => 'text',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => 2,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Body');
  t('City');
  t('Company');
  t('EventsFacts');
  t('Facility');
  t('OpenCalais Metadata: ContainedByCounty');
  t('OpenCalais Metadata: ContainedByState');
  t('OpenCalais Metadata: Latitude');
  t('OpenCalais Metadata: LegalName');
  t('OpenCalais Metadata: Longitude');
  t('OpenCalais Metadata: Ticker');
  t('Organization');
  t('Person');
  t('PhoneNumber');
  t('Position');
  t('ProvinceOrState');
  t('Tags');
  t('Tags sourced from OpenCalais about City.');
  t('Tags sourced from OpenCalais about Company.');
  t('Tags sourced from OpenCalais about Province Or State.');

  return $field_instances;
}
